<?php

use App\Http\Controllers\ArmyController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Prvu stvar koju treba da izmenis jeste da promenis sve validacije na kontrolerima i da ih stavis u zasebne fajlove.
//Drugo treba da koristis Resources za rute. !!!!!
//Trece treba da uradis ceo CRUD za sve entitete koje imas u bazi.
//Cetvrto treba da odradis dokumentaciju za API, kad budes radio front da se lakse snadjes.
//Peto treba da uradis testove, najbitniji testovi su vezani za game controller. Vidi da uradis unit, integration testove..
//Obavezno implementiraj autorizaciju za API. (VISOK PRIORITET!!!!)
//Proveri da li radi pre-commit hook za PSR standard.
//Obavezno srediti Dependency Injection preko ServiceProvidera i ServiceContainer-a
Route::post('/army/create', [ArmyController::class, "postCreate"]);
Route::post('/game/create', [GameController::class, "postCreate"]);
Route::put('/game/assign-army', [GameController::class, "postAssignArmyToGame"]);
Route::post('/game/run-attack', [GameController::class, "runAttack"]);
Route::post('/game/reset/{game}', [GameController::class, "resetBattleGame"]);

Route::get('/games', [GameController::class, "getAllGames"]);
Route::get('/armies', [ArmyController::class, "getArmies"]);
Route::get('/armies/{army}', [ArmyController::class, "find"]);
Route::post('/armies/{army}/update', [ArmyController::class, "update"]);
Route::delete('/armies/{army}/delete', [ArmyController::class, "delete"]);
Route::get('/game/{game}', [GameController::class, "getGame"]);
Route::post('/games/update/{game}', [GameController::class, 'update']);
Route::delete('/games/delete/{game}', [GameController::class, 'delete']);
Route::get('/users', [UsersController::class, 'getAllUsers']);
Route::get('/users/{user}', [UsersController::class, 'getUser']);
Route::post('/users/update/{user}', [UsersController::class, 'update']);
Route::post('/users/create', [UsersController::class, 'create']);
Route::delete('/users/delete/{user}', [UsersController::class, 'delete']);

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::middleware('auth:sanctum')->post('logout', [AuthController::class, 'logout']);

Route::get("/test", function () {
    return "PERA PERIC";
});


