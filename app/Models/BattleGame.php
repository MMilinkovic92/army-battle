<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BattleGame extends Model
{
    use HasFactory;

    static protected $MINIMAL_NUMBER_OF_ARMIES_TO_PROCEED_ATTACK = 2;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The army that belong to the battle game.
     */
    public function armies()
    {
        return $this->belongsToMany(Army::class, 'army_game', 'game_id', 'army_id')
                    ->withPivot('attack_position', 'created_at', 'updated_at')
                    ->orderBy('attack_position', 'asc');
    }

    /**
     * The army that belong to the battle game.
     */
    public function armiesWithUnorderedPositions()
    {
        return $this->belongsToMany(Army::class, 'army_game', 'game_id', 'army_id')
                    ->withPivot('attack_position', 'created_at', 'updated_at');
    }

    /**
     * Get battle logs.
     *
     * @return HasMany
     */
    public function battleLogs(): HasMany
    {
        return $this->hasMany(BattleLog::class, 'game_id', 'id');
    }

    /**
     * Check game is finished.
     *
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->status == "finished";
    }

    /**
     * Check game is started.
     *
     * @return bool
     */
    public function isStarted(): bool
    {
        return $this->status == "started";
    }

    /**
     * Check game is in progress.
     *
     * @return bool
     */
    public function isInProgress(): bool
    {
        return $this->status == "progress";
    }

    /**
     * Check game is pending.
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->status == "pending";
    }

    /**
     *  Check does game have this army.
     *
     * @param Army $army
     * @return bool
     */
    public function isArmyAssigned(Army $army): bool
    {
        return $this->armies()->where('army_id', $army->id)->exists();
    }

    /**
     * Check is first attack.
     *
     * @return bool
     */
    public function isFirstAttack(): bool
    {
        if ($this->status == "pending" || !$this->battleLogs()->exists()) {
            return true;
        }

        return false;
    }

    /**
     * Begin battle -> change status to started.
     *
     * @return void
     */
    public function beginBattle(): void
    {
        $this->status = "started";
    }

    /**
     * Decrease number of steps.
     *
     * @return void
     */
    public function decreaseStep(): void
    {
        $this->number_of_turns =  $this->number_of_turns - 1;
    }

    /**
     * Change status to finished if battle is over.
     *
     * @return void
     */
    public function changeGameStatusIfBattleIsOver(): void
    {
        if ($this->number_of_turns == 0 || $this->armies()->where('units', ">", 0)->count() == 1) {
            $this->status = "finished";
        }
    }

    /**
     * Check does it has enough armies.
     *
     * @return bool
     */
    public function hasEnoughArmiesToStartBattle(): bool
    {
        return $this->minimum_number_of_armies <= $this->armies()->count();
    }

    /**
     * Check can game proceed next attack turn.
     *
     * @return bool
     */
    public function canProceedAttackTurn(): bool
    {
        $numberOfUndestroyedArmies = $this->armies()->where('units', ">", 0)->count();

        return $numberOfUndestroyedArmies >= self::$MINIMAL_NUMBER_OF_ARMIES_TO_PROCEED_ATTACK;
    }

    /**
     * Get hash key for cache.
     *
     * @return string
     */
    public function getHashKey(): string
    {
        return hash("sha256", $this->name.$this->id);
    }
}
