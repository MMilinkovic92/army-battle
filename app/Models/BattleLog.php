<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BattleLog extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * Get Attackers army.
     *
     * @return BelongsTo
     */
    public function attacker(): BelongsTo
    {
        return $this->belongsTo(Army::class, 'attacker_id', 'id');
    }

    /**
     * Get defenders army.
     *
     * @return BelongsTo
     */
    public function defender(): BelongsTo
    {
        return $this->belongsTo(Army::class, 'defender_id', 'id');
    }
}
