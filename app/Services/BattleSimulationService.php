<?php


namespace App\Services;

use App\Http\Resources\BattleGameResource;
use App\Library\Army\ArmyManager;
use App\Library\BattleLog\BattleLogManager;
use App\Library\Game\BattleGameManager;
use App\Models\Army;
use App\Models\BattleGame;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Cache;

class BattleSimulationService
{
    protected $data;
    protected $battleGameManager;
    protected $armyManager;
    protected $battleLogManager;

    /**
     * Class constructor.
     *
     * BattleSimulationService constructor.
     * @param BattleGameManager $battleGameManager
     * @param ArmyManager $armyManager
     * @param BattleLogManager $battleLogManager
     */
    public function __construct(
        BattleGameManager $battleGameManager,
        ArmyManager $armyManager,
        BattleLogManager $battleLogManager
    ) {
        $this->battleGameManager = $battleGameManager;
        $this->armyManager = $armyManager;
        $this->battleLogManager = $battleLogManager;
    }

    /**
     * Start turn.
     *
     * @return void
     */
    public function startTurn(): void
    {
        $game = BattleGame::find($this->data['game_id']);

        if ($game->isFirstAttack()) {
            $this->saveInitialBattlegroundSetting($game);
        }

        foreach ($game->armies()->alive()->get() as $army) {
            $this->attack($army, $game);
        }
    }

    /**
     * End turn, do all things at the end of turn. Reload units, reorder positions etc.
     *
     * @return void
     */
    public function endTurn(): void
    {
        $game = BattleGame::find($this->data['game_id']);

        $this->armyManager->setGame($game)->reloadUnits();
        $this->battleGameManager->setGame($game)->reorderPositions()->updateGameStatus();
    }

    /**
     * Check is battle game finished.
     *
     * @return bool
     */
    public function isBattleFinished(): bool
    {
        //Ocisti kes ako je gotova bitka.
        return BattleGame::find($this->data['game_id'])->isFinished();
    }

    /**
     * Reset battle ground.
     *
     * @param  BattleGame $game
     * @return void
     */
    public function resetBattleGround(BattleGame $game): void
    {
        //Cache treba smestiti u odvojene metode, mozda cak i klasu, getHashKey() -> rename.
        $initialSettings = Cache::get($game->getHashKey());

        //metoda treba da vraca true ili false, refactor.
        if ($initialSettings) {
            $gameToBeReset = BattleGame::find($initialSettings->game->id);

            //ovde treba interfejs i da se ove metode resetToInitial malo preprave. Refactor
            $this->armyManager->setGame($gameToBeReset)->resetToInitial($initialSettings->armies);
            $this->battleGameManager->setGame($gameToBeReset)->resetToInitial($initialSettings->game);
            $this->battleLogManager->resetToInitial($gameToBeReset);
            Cache::forget($game->getHashKey());
        }
    }

    /**
     * Get winner army.
     *
     * @return Army
     */
    public function getWinner(): Army
    {
        $game = BattleGame::find($this->data['game_id']);
        return $this->battleGameManager->getWinnerOfTheGame($game);
    }

    /**
     * Get all battle logs.
     *
     * @return JsonResource
     */
    public function getBattleStatistics(): JsonResource
    {
        return new BattleGameResource(BattleGame::find($this->data['game_id']));
    }

    /**
     * Attack another army based on strategy.
     *
     * @param  Army $attackerArmy
     * @param  BattleGame $game
     * @return bool
     */
    private function attack(Army $attackerArmy, BattleGame $game): bool
    {
        $defenderArmy = $this->battleGameManager->setArmy($attackerArmy)->setGame($game)->getArmyBasedOnStrategy();

        $canAttack = $this->armyManager->setArmy($attackerArmy)->canAttackSuccessfully();

        $dataLog = [
            "game_id" => $game->id,
            "attacker_id" => $attackerArmy->id,
            "defender_id" => $defenderArmy->id,
            "is_succeed" => $canAttack,
            "units_before_attack" => $defenderArmy->units,
        ];

        if ($canAttack) {
            $damage = $this->armyManager->calculateDamage();
            $dataLog["damage_dealt"] = $damage;
            $this->armyManager->setArmy($defenderArmy)->receiveDamage($damage);
        }
        $dataLog["units_after_attack"] = $defenderArmy->units;

        return $this->battleLogManager->setData($dataLog)->store();
    }

    /**
     * Save initial battleground settings in cache.
     *
     * @param  BattleGame $game
     * @return void
     */
    private function saveInitialBattlegroundSetting(BattleGame $game): void
    {
        $init = (object)[
            "game" => $game,
            "armies" => $game->armies
        ];

        if (!Cache::has($game->getHashKey())) {
            Cache::put($game->getHashKey(), $init, 600);
        }
    }

    /**
     * Set data.
     *
     * @param  mixed $data
     * @return self
     */
    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }
}
