<?php


namespace App\Library\Game;

use App\Models\Army;
use App\Models\BattleGame;
use Illuminate\Support\Facades\DB;

class BattleGameManager
{
    protected $data;
    protected $game;
    protected $army;

    /**
     * Create game.
     *
     * @return int
     */
    public function create(): int
    {
        $game = BattleGame::create($this->data);

        return ($game) ? $game->id : 0;
    }

    /**
     * Assign an army to the game.
     *
     * @return bool
     */
    public function assignArmy(): bool
    {
        $game = BattleGame::find($this->data['game_id']);
        $armies = Army::whereIn('id', $this->data['army_ids']);

//        if ($game->isFinished() || !$armies->exists() || $game->isArmyAssigned($army->first())) {
//            return false;
//        }

        DB::transaction(function () use ($game) {
//            $this->updatePositions($game);
            $game->armies()->attach($this->data['army_ids']);
        });

        return true;
    }

    /**
     * Check, can start game or attack.
     *
     * @return bool
     */
    public function canStartAttack(): bool
    {
        $game = BattleGame::find($this->data['game_id']);

        if ($game->isFirstAttack()) {
            return $game->hasEnoughArmiesToStartBattle();
        }

        return $game->canProceedAttackTurn();
    }

    /**
     * Get army based on attacker strategy.
     *
     * @return Army
     */
    public function getArmyBasedOnStrategy(): Army
    {
        $method = "get".ucfirst($this->army->attack_strategy)."Army";

        return $this->{$method}();
    }

    /**
     * Set game fields.
     * @return void
     */
    public function updateGameStatus(): void
    {
        if ($this->game->isPending()) {
            $this->game->beginBattle();
        }
        $this->game->decreaseStep();
        $this->game->changeGameStatusIfBattleIsOver();
        $this->game->save();
    }

    /**
     * Reorder positions after every turn.
     *
     * @return $this
     */
    public function reorderPositions(): self
    {
        $allArmies = $this->game->armiesWithUnorderedPositions()->count();

        foreach ($this->game->armiesWithUnorderedPositions as $army) {
            $army->pivot->attack_position = ($army->pivot->attack_position % $allArmies) + 1;
            $army->pivot->save();
        }

        return  $this;
    }

    /**
     * Get winner of the game.
     *
     * @param BattleGame $game
     * @return Army
     */
    public function getWinnerOfTheGame(BattleGame $game): Army
    {
        return $game->armies()->where('units', '>', '0')->first();
    }

    /**
     * Reset game to initial battleground settings
     *
     * @param BattleGame $initialGame
     * @return void
     */
    public function resetToInitial(BattleGame $initialGame): void
    {
        $this->game->number_of_turns = $initialGame->number_of_turns;
        $this->game->status =  $initialGame->status;
        $this->game->save();
    }

    /**
     * Return all Battles.
     *
     * @return BattleGame[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllBattles()
    {
        return BattleGame::all();
    }

    /**
     * Increment position.
     *
     * @param BattleGame $game
     * @return void
     */
    private function updatePositions(BattleGame $game): void
    {
        $game->armies()->increment('attack_position', 1);
    }

    /**
     * Find the weakest, if we have more than one weakest, choose firstly created.
     *
     * @return Army
     */
    private function getWeakestArmy(): Army
    {
        return  $this->game->armiesWithUnorderedPositions()
                           ->alive()
                           ->whereKeyNot($this->army->id)
                           ->orderBy('units', 'asc')
                           ->first();
    }

    /**
     * Find the strongest, if we have more than one strongest, choose firstly created.
     *
     * @return Army
     */
    private function getStrongestArmy(): Army
    {
        return  $this->game->armiesWithUnorderedPositions()
                            ->alive()
                            ->whereKeyNot($this->army->id)
                            ->orderBy('units', 'desc')
                            ->first();
    }

    /**
     * Find random army.
     *
     * @return Army
     */
    private function getRandomArmy(): Army
    {
        $aliveArmiesInGame = $this->game->armies()->whereKeyNot($this->army->id)->alive();
        $numberOfArmies = $aliveArmiesInGame->count();
        $randomArmyPosition = rand(0, $numberOfArmies - 1);

        return $aliveArmiesInGame->get()[$randomArmyPosition];
    }

    /**
     * Set game.
     *
     * @param BattleGame $game
     * @return self
     */
    public function setGame($game): self
    {
        $this->game = $game;
        return $this;
    }

    /**
     * Set Army
     *
     * @param mixed $army
     * @return self
     */
    public function setArmy($army): self
    {
        $this->army = $army;
        return $this;
    }

    /**
     * Set mass assign data.
     *
     * @param mixed $data
     * @return self
     */
    public function setData($data): self
    {
        $this->data = $data;
        return $this;
    }
}
