<?php


namespace App\Library\BattleLog;

use App\Models\Army;
use App\Models\BattleGame;
use App\Models\BattleLog;

class BattleLogManager
{
    protected $data;

    /**
     * Store battle log statistic.
     *
     * @return bool
     */
    public function store(): bool
    {
        $battleLog = null;
        $isAlive = Army::find($this->data['attacker_id'])->units > 0;

        if ($isAlive) {
            $battleLog = BattleLog::create($this->data);
        }

        return ($battleLog) ? true : false;
    }

    /**
     * Remove logs for game.
     *
     * @param BattleGame $game
     * @return void
     */
    public function resetToInitial(BattleGame $game): void
    {
        $game->battleLogs()->delete();
    }

    /**
     * @param mixed $data
     * @return self
     */
    public function setData($data): self
    {
        $this->data = $data;
        return $this;
    }
}
