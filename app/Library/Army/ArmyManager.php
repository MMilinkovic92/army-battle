<?php


namespace App\Library\Army;

use App\Models\Army;
use App\Models\BattleGame;
use Illuminate\Database\Eloquent\Collection;

class ArmyManager
{
    static protected $SUCCESS_RATE_PERCENTAGE_PER_UNIT = 1;
    static protected $DAMAGE_PER_UNIT = 0.5;
    static protected $UNITS_PER_CENTISECOND = 1;
    static protected $RELOAD_TIME_IN_CENTISECONDS = 10;
    protected $data;
    protected $army;
    protected $game;

    /**
     * Create army.
     *
     * @return bool
     */
    public function create(): bool
    {
        $army = Army::create($this->data);

        return ($army) ? true : false;
    }

    /**
     * Check does army can attack another army.
     *
     * @return bool
     */
    public function canAttackSuccessfully(): bool
    {
        //Ovo je metoda za model.
        $armyChance = $this->army->units * self::$SUCCESS_RATE_PERCENTAGE_PER_UNIT;
        $calculateRandomChance = intval(mt_rand() / mt_getrandmax() * 100);

        return $armyChance > $calculateRandomChance;
    }

    /**
     * Calculate army damage.
     *
     * @return int
     */
    public function calculateDamage(): int
    {
        //Ovo je metoda za model.
        return ($this->army->units > 1) ? intval($this->army->units * self::$DAMAGE_PER_UNIT) : 1;
    }

    /**
     * Defender army receive damage.
     *
     * @param int $damage
     */
    public function receiveDamage(int $damage): void
    {
        //Ovo je metoda za model.
        $damage = $this->army->units - $damage;
        $damage = ($damage > 0) ? $damage : 0;

        $this->army->update(['units' => $damage]);
    }

    /**
     * Reload units.
     *
     * @return void
     */
    public function reloadUnits(): void
    {
        $this->game
             ->armies()
             ->alive()
             ->increment('units', self::$UNITS_PER_CENTISECOND * self::$RELOAD_TIME_IN_CENTISECONDS);
    }

    /**
     * Reset values to initial stage.
     *
     * @param Collection $armies
     * @return void
     */
    public function resetToInitial(Collection $armies): void
    {
        foreach ($this->game->armies as $army) {
            $this->changeArmyValues($army, $armies->where('id', $army->id)->first());
        }
    }

    /**
     * Change army values to init configuration.
     *
     * @param $currentStateArmy
     * @param $initialArmy
     * @return void
     */
    private function changeArmyValues($currentStateArmy, $initialArmy): void
    {
        $currentStateArmy->units = $initialArmy->units;
        $currentStateArmy->pivot->attack_position = $initialArmy->pivot->attack_position;
        $currentStateArmy->save();
        $currentStateArmy->pivot->save();
    }

    /**
     * @param mixed $game
     * @return self
     */
    public function setGame($game): self
    {
        $this->game = $game;
        return $this;
    }

    /**
     * Set army.
     *
     * @param Army $army
     * @return self
     */
    public function setArmy($army): self
    {
        $this->army = $army;
        return $this;
    }

    /**
     * Set data.
     *
     * @param array $data
     * @return self
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }
}
