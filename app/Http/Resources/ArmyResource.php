<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArmyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'              => $this->name,
            'units'             => $this->units,
            'attack_strategy'   => $this->attack_strategy,
            'position'          => $this->pivot->attack_position
        ];
    }
}
