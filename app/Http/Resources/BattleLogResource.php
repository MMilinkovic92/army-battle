<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BattleLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "attacker" => $this->attacker,
            "defender" => $this->defender,
            "damage_dealt" => $this->damage_dealt,
            "units_before_attack" => $this->units_before_attack,
            "units_after_attack" => $this->units_after_attack,
            "is_succeed" => $this->is_succeed,
            "turn_number" => $this->turn_number
        ];
    }
}
