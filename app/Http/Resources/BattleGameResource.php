<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BattleGameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'name'                      => $this->name,
            'number_of_turns'           => $this->number_of_turns,
            'minimum_number_of_armies'  => $this->minimum_number_of_armies,
            'status'                    => $this->status,
            'armies'                    => ArmyResource::collection($this->armies),
            'battleLogs'                => BattleLogResource::collection($this->battleLogs)
        ];
    }
}
