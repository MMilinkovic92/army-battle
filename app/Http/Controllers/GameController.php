<?php

namespace App\Http\Controllers;

use App\Http\Resources\BattleGameResource;
use App\Library\Game\BattleGameManager;
use App\Models\Army;
use App\Models\BattleGame;
use App\Services\BattleService;
use App\Services\BattleSimulationService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class GameController extends Controller
{
    protected $gameManager;
    protected $battleService;
    protected $winner = null;

    public function __construct(BattleGameManager $gameManager, BattleSimulationService $battleService)
    {
        $this->gameManager = $gameManager;
        $this->battleService = $battleService;
    }

    /**
     * Create game.
     *
     * @return Application|ResponseFactory|Response
     */
    public function postCreate()
    {
        $validation = Validator::make(request()->all(), [
            'name' => 'required|max:40',
            'number_of_turns' => 'required|integer|between:40,80',
            'minimum_number_of_armies' => 'required|integer|min:5'
        ]);

        if ($validation->fails()) {
            return response(['errors' => $validation->errors()], 400);
        }

        $idGame = $this->gameManager->setData(request()->all())->create();

        if (!$idGame) {
            return response(['error' => "Unable to create, please try again"], 500);
        }

        return response(['message' => 'Successful create', 'id_game' => $idGame], 201);
    }

    /**
     * Assign army to the game.
     *
     * @return Application|ResponseFactory|Response
     */
    public function postAssignArmyToGame()
    {
        $validation = Validator::make(request()->all(), [
            'army_ids' => 'required|array',
            'game_id' => 'required|integer|exists:battle_games,id',
        ]);

        if ($validation->fails()) {
            return response(['errors' => $validation->errors()], 400);
        }

        $isSuccessful = $this->gameManager->setData(request()->all())->assignArmy();

        if (!$isSuccessful) {
            return response(['error' => "Unable to assign army, please try again"], 400);
        }

        return response(['message' => 'Successfully assigned'], 201);
    }

    /**
     * Run attack based on game setup.
     *
     * @return Application|ResponseFactory|Response
     */
    public function runAttack()
    {
        $validation = Validator::make(request()->all(), [
            'game_id' => "required|integer|exists:battle_games,id"
        ]);

        if ($validation->fails()) {
            return response(['errors' => $validation->errors()], 400);
        }

        if (!$this->gameManager->setData(request()->all())->canStartAttack()) {
            return response(['errors' => "Can not start attack"], 400);
        }

        $this->battleService->setData(request()->all())->startTurn();
        $this->battleService->endTurn();

        if ($this->battleService->isBattleFinished()) {
            $this->winner = $this->battleService->getWinner();
        }

        return response(
            [
            'message' => 'Successful turn',
            'winner' => $this->winner,
            'game' => $this->battleService->getBattleStatistics()
            ],
            201
        );
    }

    /**
     * Set all game specification on game startup before first attack.
     *
     * @param BattleGame $game
     * @return Application|ResponseFactory|Response
     */
    public function resetBattleGame(BattleGame $game)
    {
        //fali mi cache clear kada zavrsim resetovanje
        $this->battleService->resetBattleGround($game);
        return response(['message' => 'Successfully reset'], 201);
    }

    /**
     * Get all games and game logs.
     *
     * @return Application|ResponseFactory|Response
     */
    public function getAllGames()
    {
        return response(['games' => BattleGameResource::collection($this->gameManager->getAllBattles())], 200);
    }

    /**
     * Get single battle game.
     *
     * @param BattleGame $game
     * @return Application|ResponseFactory|Response
     */
    public function getGame(BattleGame $game)
    {
        return response(['game' => $game], 200);
    }

    /**
     * Update game.
     *
     * @param BattleGame $game
     */
    public function update(BattleGame $game)
    {
        try {
            $game->update(\request()->all());
        } catch (\Exception $exception) {
            return \response(['message' => $exception->getMessage()]);
        }

        return \response(['message' => 'Successful update', 'game' => $game->fresh()]);
    }

    /**
     * Delete game.
     *
     * @param BattleGame $game
     */
    public function delete(BattleGame $game)
    {
        try {
            $game->delete();
        } catch (\Exception $exception) {
            return \response(['message' => $exception->getMessage()]);
        }

        return \response(['message' => 'Successful delete']);
    }
}
