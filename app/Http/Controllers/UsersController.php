<?php

namespace App\Http\Controllers;

use App\Models\User;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    /**
     * Get user.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getUser(User $user)
    {
        return response(['user' => $user]);
    }

    /**
     * Get all users.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getAllUsers()
    {
        $users = User::all();

        return response(['users' => $users]);
    }

    /**
     * Update user.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(User $user)
    {
        $params = \request()->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'email' => ['required'],
            'password' => ['string', 'nullable']
        ]);

        if (!empty(request()->get('password'))) {
            $params['password'] = \Hash::make($params['password']);
        } else {
            unset($params['password']);
        }

        try {
            $user->update($params);
        } catch (\Exception $exception) {
            return response(['message' => $exception->getMessage()]);
        }

        return response(['message' => 'Successful update']);
    }

    /**
     * Create user.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function create()
    {
        $params = \request()->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'email' => ['required'],
            'password' => ['required'],
        ]);

        $params['password'] = \Hash::make($params['password']);

        try {
            User::create($params);
        } catch (\Exception $exception) {
            return response(['message' => $exception->getMessage()]);
        }

        return response(['message' => "Successful create."]);
    }

    /**
     * Delete user.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function delete(User $user)
    {
        try {
            $user->delete();
        } catch (\Exception $exception) {
            return response(['message' => $exception->getMessage()]);
        }

        return response(['message' => 'Successful delete']);
    }

}
