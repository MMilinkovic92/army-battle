<?php

namespace App\Http\Controllers;

use App\Library\Army\ArmyManager;
use App\Models\Army;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ArmyController extends Controller
{
    protected $armyManager;

    public function __construct(ArmyManager $armyManager)
    {
        $this->armyManager = $armyManager;
    }

    /**
     * Create army.
     *
     * @return Application|ResponseFactory|Response
     */
    public function postCreate()
    {
        $validation = Validator::make(request()->all(), [
            'name' => 'required|max:40',
            'units' => 'required|integer|between:80,100',
            'attack_strategy' => ['required', Rule::in(['random', 'weakest', 'strongest'])]
        ]);

        if ($validation->fails()) {
            return response(['errors' => $validation->errors()], 400);
        }

        $isSuccessful = $this->armyManager->setData(request()->all())->create();

        if (!$isSuccessful) {
            return response(['error' => "Unable to create, please try again"], 500);
        }

        return response(['message' => 'Successful create'], 200);
    }

    /**
     * Get all armies.
     *
     * @return Application|ResponseFactory|Response
     */
    public function getArmies()
    {
        $armies = Army::all();

        return response(['armies' => $armies]);
    }

    /**
     * Find army.
     *
     * @param Army $army
     * @return Application|ResponseFactory|Response
     */
    public function find(Army $army)
    {
        return \response(['army' => $army]);
    }

    /**
     * Update army.
     *
     * @param Army $army
     * @return Application|ResponseFactory|Response
     */
    public function update(Army $army)
    {
        $army->update(request()->all());
        $army->fresh();

        return \response(['army' => $army]);
    }

    /**
     * Delete army.
     *
     * @param Army $army
     * @return Application|ResponseFactory|Response
     * @throws \Exception
     */
    public function delete(Army $army)
    {
        try {
            $army->delete();
        } catch (\Exception $exception) {
            return \response(['message' => "Cannot delete army, it exists in battle logs."]);
        }

        return \response(['message' => "Successfully deleted"]);
    }
}
