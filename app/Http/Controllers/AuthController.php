<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\TokenGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function Login()
    {
        $credentials = \request()->validate([
            'email' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $token = Auth::user()->createToken(\Str::random());

            return response(['message' => 'Successful login.', 'token' => $token->plainTextToken]);
        }

        return response(['message' => "Unsuccessful login"]);
    }

    public function logout()
    {
        \request()->user()->tokens()->delete();
    }

    public function register()
    {
        $params = \request()->validate([
           'first_name' => ['required'],
           'last_name' => ['required'],
           'email' => ['required'],
           'password' => ['required'],
        ]);

        $params['password'] = \Hash::make($params['password']);

        $user = User::create($params);

        return response(['message' => "Successful register.", 'token' => $user->createToken(\Str::random())->plainTextToken]);
    }
}
