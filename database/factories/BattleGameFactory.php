<?php

namespace Database\Factories;

use App\Models\BattleGame;
use Illuminate\Database\Eloquent\Factories\Factory;

class BattleGameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BattleGame::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->country." battle ground",
            'number_of_turns' => rand(40, 60),
            'minimum_number_of_armies' => 5
        ];
    }
}
