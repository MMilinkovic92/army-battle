<?php

namespace Database\Factories;

use App\Models\Army;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArmyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Army::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $attackStrategy = ['random', 'weakest', 'strongest'];
        return [
            'name' => $this->faker->name." battle army",
            'units' => rand(80, 100),
            'attack_strategy' => $attackStrategy[rand(0, 2)]
        ];
    }
}
