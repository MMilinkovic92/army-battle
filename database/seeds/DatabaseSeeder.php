<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(create_default_number_of_armies::class);
        $this->call(create_default_number_of_games::class);
        $this->call(\Database\Seeders\create_default_number_of_users::class);
    }
}
