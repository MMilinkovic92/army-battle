<?php

use App\Models\Army;
use Illuminate\Database\Seeder;

class create_default_number_of_armies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Army::factory(10)->create();
    }
}
