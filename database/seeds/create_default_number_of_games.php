<?php

use App\Models\BattleGame;
use Illuminate\Database\Seeder;


class create_default_number_of_games extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BattleGame::factory(10)->create();
    }
}
