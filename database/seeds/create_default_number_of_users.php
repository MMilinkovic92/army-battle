<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class create_default_number_of_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
    }
}
