<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBattleLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battle_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique()->autoIncrement();
            $table->unsignedBigInteger('game_id');
            $table->unsignedBigInteger('attacker_id');
            $table->unsignedBigInteger('defender_id');
            $table->unsignedBigInteger('damage_dealt')->default(0);
            $table->unsignedBigInteger('units_before_attack')->default(0);
            $table->unsignedBigInteger('units_after_attack')->default(0);
            $table->boolean('is_succeed')->default(false);
            $table->unsignedBigInteger('turn_number')->default(1);
            $table->timestamps();

            $table->foreign('game_id')->references('id')->on('battle_games');
            $table->foreign('attacker_id')->references('id')->on('armies');
            $table->foreign('defender_id')->references('id')->on('armies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battle_logs');
    }
}
