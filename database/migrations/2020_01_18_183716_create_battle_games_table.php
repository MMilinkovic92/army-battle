<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBattleGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battle_games', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique()->autoIncrement();
            $table->string('name');
            $table->unsignedInteger('number_of_turns');
            $table->unsignedInteger('minimum_number_of_armies');
            $table->enum('status', ['pending', 'started', 'progress', 'finished'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battle_games');
    }
}
