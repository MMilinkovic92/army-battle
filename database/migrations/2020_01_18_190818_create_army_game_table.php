<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArmyGameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('army_game', function (Blueprint $table) {
            $table->unsignedBigInteger('army_id');
            $table->unsignedBigInteger('game_id');
            $table->unsignedInteger('attack_position')->default(1);
            $table->timestamps();

            $table->foreign('army_id')->references('id')->on('armies')->onDelete('cascade');
            $table->foreign('game_id')->references('id')->on('battle_games')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('army_game');
    }
}
