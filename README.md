## Instructions

This is simple application game. Steps for setup application: 

- Use Laravel Homestead (this is optional)
- Download project.
- Run composer install.
- Run migrations.
- Run seeders.
- You can check app by postman routes.
- Change config/cors.php => * 'paths' => ['api/*'] * => this will allow frontend app to call API routes.
- Download fronted app. **(https://bitbucket.org/MMilinkovic92/frontend-army-battle/src/master/)**
- In a file TransactionManager.js you have baseRoute variable -> set yours local config.
- Use UI of frontend app.
